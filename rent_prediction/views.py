import pickle

from django.shortcuts import render

model = pickle.load(open('rent_model.pkl', 'rb'))


def predict(request):
    return render(request, "predict.html")


def result(request):
    # if request.method == 'GET':
    v0 = str(request.GET['n0'])
    v1 = int(request.GET['n1'])
    v2 = int(request.GET['n2'])
    v3 = int(request.GET['n3'])
    v4 = int(request.GET['n4'])
    v5 = float(request.GET['n5'])

    y = [[v0, v1, v2, v3, v4, v5]]
    rent = model.predict(y)
    rent_1 = round(rent[0], 2)

    return render(request, "predict.html", {"predicted_rent": 'Predicted Rent:: Nu.' + format(rent_1)})
