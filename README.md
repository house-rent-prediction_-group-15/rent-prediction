# House Rent Prediction Using Machine Learning

## *Gyalpozging College of Information Technology (Bachelor of Scinece in Information Technology) Year 3 Project*


House Rental Prediction is a web application that predicts the rent of a government cottages in Bhutan. This application predicts the rent of houses based on the location in which dzongkhag the house is located, number of bedrooms, living rooms, kitchens and bathrooms, and the square feet of the house. This is convenient for those who are moving to a new location since it gives the government approved rent and can help them avoid scammers. This application is available to all who resides in Bhutan.

## AIM
The project aims to create a model to predict the rent of houses considering various variables describing the aspects of residential houses.

The website is deployed in Heroku, check out the website ([house rent prediction. ](http://gcit-rent.herokuapp.com/))

#### Built using: Python, HTML, CSS, Bootstrap, JavaScript.
#### Framework: Django

## Video

You can find the promotional video in the youtube. ([YouTube. ](https://youtu.be/ejvgigvXN-c))

## Website Screeenshot

![](/Images/one.png)

![](/Images/two.png)

![](/Images/three.png)

![](/Images/four.png)

![](/Images/five.png)

## Poster

![](/Images/poster.jpeg)
